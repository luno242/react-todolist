import React, {Component} from 'react';

class TodoList extends Component {
    constructor() {
        super();
        this.state = {
            userInput:'',//imput zustane prazdny 
            items: [] // pro vyplneni tabulky
        };
    }
    onChange(event) {
        //pro update vzdycky, kdyz jdu do todolist
        this.setState({
            userInput: event.target.value // pridame hodnotu  userInput - pro vyplneni 
        }); // pro potvrzeni zda travailler
    }

    addTodo(event){ //pro pridani item do formulare, v button na funkci onClick prida item
        event.preventDefault(); //kdyz nepridame tento radek stranka udela automaticky reload a neprida item
        this.setState({
            userInput: '', //kdyz zadam hodnotu do input a potvrdim, zustane pote input prazdny
            items: [...this.state.items, this.state.userInput] // reprendre table origin/userInput prida novou pridanou hodnotu
        },() => console.log(this.state));
    }

    deleteTodo(event){ // delete item
        event.preventDefault();
        const array = this.state.items;
        const index = array.indexOf(event.target.value); //vezmu index intem, ktere chci delete
        array.splice(index,1); //delete par splice pomoci index, ktery vybere jeden index 
        this.setState({
            items: array
        });
    }


   
    renderTodos() { //abych videla hodnoty,ktere jsem zadala do todolistu
        return this.state.items.map((item) => { // map mapuje items a udela loop kazdemu pridanemu item ((item)=value)  
            return ( //{item} my umozni prepisovat hodnotu vzdy novym slovem
                <div className="list-group-item" key={item}>
                    {item} <button className="btn btn-outline-danger" onClick={this.deleteTodo.bind(this)}>X</button> 
                </div>
            );
        });
    }

    render() {
        return (
            <div>
                <h1 align='center'>Ma Todo List</h1>
                <form className='form-row align-items-center'>
                    <input 
                    value={this.state.userInput} //dala jsem hodnotu 
                    type="text" 
                    placeholder="Renseigner un item"
                    onChange={this.onChange.bind(this)} //vzdy kdyz zmenim hodnotu aktualizuje mi objekt this
                    className='form-control mb-2'
                    />
                    <button 
                    onClick={this.addTodo.bind(this)}
                    className="btn btn-primary"
                    >
                    Ajouter
                    </button> 
                </form>
                <div className="list-group">
                    {this.renderTodos()}
                </div>
            </div>
        ); 
    }
}
export default TodoList;